---
layout:     single
title:      "About"
date:       2016-11-22T19:00:00-05:00
permalink:  /about/
sidebar:
  nav:  "about"
---
Welcome to Collegi, the Minecraft survival server where you can catch, trade,
battle, and use pokemon to help you build your world! Far from vanilla, our
server uses a combination of mods like Pixelmon and Biomes ‘o Plenty to make
the richest, most fun Minecraft experience we can. With a fully fledged
economy, town system, gyms, infinite mining worlds and more, Collegi is a
Pixelmon server that is more than just playing Pokemon in the world of
Minecraft. It’s a Minecraft server where your Pokemon are your partners,
helping you survive, build, and thrive.
